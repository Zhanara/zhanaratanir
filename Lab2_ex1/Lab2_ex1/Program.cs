﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2_ex1
{
    class Program
    {
        static void ShowDirectory(DirectoryInfo dir)
        {
            // Show Each File
            foreach (FileInfo file in dir.GetFiles())
            {
                Console.WriteLine("File: {0}", file.FullName);
            }
            // Go through subdirectories
            // recursively
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                ShowDirectory(subDir);
            }

        }

        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(@"C:\study");
            ShowDirectory(dir);
            Console.ReadKey();

        }
    }
}
