﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    struct Point
    {
        public int x, y;
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point();
            a.x = 5;
            a.y = 6;
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
