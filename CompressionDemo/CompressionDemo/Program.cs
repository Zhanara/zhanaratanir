﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;


namespace CompressionDemo
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream Sourcefile = File.OpenRead(inFilename);
            FileStream destFile = File.Create(outFilename);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = Sourcefile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = Sourcefile.ReadByte();
            }


        }

        static void UnCompressFile(string inFilename, string outFilename)
        {
            FileStream Sourcefile = File.OpenRead(inFilename);
            FileStream destFile = File.Create(outFilename);
            GZipStream compStream = new GZipStream(Sourcefile, CompressionMode.Decompress);
            int theByte = Sourcefile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = Sourcefile.ReadByte();
            }


        }
        static void Main(string[] args)
        {
            CompressFile(@"c:\boot.ini", @"c:\boot.ini.gz");
            UnCompressFile(@"c:\boot.ini.gz", @"c:\boot.ini.test");
        }
    }
}
