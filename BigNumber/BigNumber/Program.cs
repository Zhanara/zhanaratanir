﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    struct BN
    {
         public int[] a;
         public int size;

         public BN(string s)
         {
             a = new int[1000];
             size = s.Length;
             for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
             {
                 a[j] = s[i] - '0';
             }
         }

         public static BN operator +(BN arg1, BN arg2)
         {
             BN result = new BN("");
             int NewSize = Math.Max(arg1.size, arg2.size); // наибольшее из двух величин;
             for (int i = 0; i < NewSize; i++)
             {
                 result.a[i] = arg1.a[i] + arg2.a[i];
                 if (result.a[i] > 9)
                 {
                     result.a[i] = result.a[i] % 10;
                     arg1.a[i+1] += 1;

                     if (i == NewSize - 1)
                          NewSize++;                     
                }
                result.size++;
             }
             
             return result;
         }
        
        public override string ToString()
         {
             string s = "" ;
             for (int i = size - 1; i >=0; i--)
             {
                 s = s + a[i].ToString();
             }
             return s;
         }
 }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the first value");
            string s1 = Console.ReadLine();
            Console.WriteLine("Enter the second value");
            string s2 = Console.ReadLine();
            BN b = new BN(s1);
            BN c = new BN(s2);
            BN Sum = b + c;
            Console.WriteLine("The sum of two values: ");
            Console.WriteLine(Sum);
            Console.ReadKey();
        }
    }
}

