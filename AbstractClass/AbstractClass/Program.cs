﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
     public abstract class Animal
        {
            public string Name { get; set;}
            public int Weight { get; set;}

            public abstract void Voice();
        }

      public class Cat : Animal
         {
             public override void Voice()
             {
                 Console.WriteLine("Meow");
             }
         }

      public class Dog : Animal
         {
             public override void Voice()
             {
                 Console.WriteLine("Gav");
             }
         }

    class Program
    {
         static void View(Animal animals) 
         {
             Console.WriteLine(animals.Name);
             Console.WriteLine(animals.Weight);
         }
        
        static void Main(string[] args)

        {
            Cat myCat = new Cat();
            myCat.Name = "Murka";
            myCat.Weight = 3;

            View(myCat);
            myCat.Voice();


            Dog myDog = new Dog();
            myDog.Name = "Tuzik";
            myDog.Weight = 8;

            View(myDog);
            myDog.Voice();

            Console.ReadKey();
        }
    }
}
