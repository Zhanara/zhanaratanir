﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    struct Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public void AddNumber(int z)
        {
            x += z;
            y += z;
        }

        public static Point operator + (Point arg1, int arg2)
        {
            arg1.x += arg2;
            arg1.y += arg2;
            return arg1;

        }

        public static Point operator +(Point arg1, Point arg2)
       {
            arg1.x += arg2.x;
            arg1.y += arg2.y;
            return arg1;
       }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(5, 6);
            Console.WriteLine(a);
            a.AddNumber(5);
            Console.WriteLine(a);
            a = a + 5;
            Console.WriteLine(a);
            Point b = new Point(1, 3);
            Point c = a + b;
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
